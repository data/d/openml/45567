# OpenML dataset: hcdr_main

https://www.openml.org/d/45567

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Home Credit Default Risk Main Table

**WARNING:** This is only the main table of the competition' training dataset! Please do not use it alone (but rather use all data available on Kaggle) unless you aim to reproduce the results of:

> Huang, X., Khetan, A., Cvitkovic, M., & Karnin, Z. (2020). 
> Tabtransformer: Tabular data modeling using contextual embeddings. 
> arXiv preprint arXiv:2012.06678v1.

Check the [Kaggle competition website](https://www.kaggle.com/competitions/home-credit-default-risk) for further information.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45567) of an [OpenML dataset](https://www.openml.org/d/45567). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45567/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45567/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45567/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

